import { EslintBuilderOptions } from './eslint.impl';
import { JsonObject, workspaces } from '@angular-devkit/core';
import { Architect } from '@angular-devkit/architect';
import { getTestArchitect } from '../utils/testing';

jest.mock('eslint');

const {CLIEngine} = require('eslint');

const mockExecuteOnFiles = jest.fn();
const mockGetFormatter = jest.fn();

CLIEngine.mockImplementation(() => {
  return {
    executeOnFiles: mockExecuteOnFiles,
    getFormatter: mockGetFormatter
  };
});

describe('EslintBuilder', () => {
  let testOptions: EslintBuilderOptions & JsonObject;
  let architect: Architect;

  beforeEach(() => {
    mockExecuteOnFiles.mockReset();
    mockGetFormatter.mockReset();
  });

  beforeEach(async () => {
    [architect] = await getTestArchitect();

    testOptions = {};

    spyOn(workspaces, 'readWorkspace').and.returnValue({
      workspace: {
        projects: {
          get: () => ({
            sourceRoot: '/root/apps/nodeapp/src'
          })
        }
      }
    });
  });

  beforeEach(() => {
    mockExecuteOnFiles.mockImplementation(files => {
      return {
        errorCount: 0,
        results: []
      };
    });

    mockGetFormatter.mockImplementation(() => {
      return () => {
        return 'All lint passed';
      };
    });
  });

  it('should run eslint', async () => {
    mockExecuteOnFiles.mockImplementation(files => {
      return {
        errorCount: 0,
        results: []
      };
    });

    mockGetFormatter.mockImplementation(() => {
      return () => {
        return 'All lint passed';
      };
    });

    const run = await architect.scheduleBuilder(
      '@vancast/builders:eslint',
      {
        ...testOptions,
        files: ['test/**/*.spec.js']
      }
    );

    const event = await run.output
      .toPromise();
    await run.stop();

    expect(mockExecuteOnFiles).toHaveBeenCalledWith(['test/**/*.spec.js']);
    expect(event.success).toBe(true);
  });

  it('should output lint fixes', async () => {
    const run = await architect.scheduleBuilder(
      '@vancast/builders:eslint',
      {
        ...testOptions,
        fix: true
      }
    );

    const event = await run.output
      .toPromise();
    await run.stop();

    expect(CLIEngine.outputFixes).toHaveBeenCalled();
    expect(event.success).toBe(true);
  });
});
