import {
  BuilderContext,
  BuilderOutput,
  createBuilder
} from '@angular-devkit/architect';
import { JsonObject, workspaces } from '@angular-devkit/core';
import { from, Observable, of } from 'rxjs';
import {CLIEngine} from 'eslint';
import { NodeJsSyncHost } from '@angular-devkit/core/node';
import { map } from 'rxjs/operators';

export interface EslintBuilderOptions {
  fix?: boolean;
  files?: string[];
}

export default createBuilder<JsonObject & EslintBuilderOptions>(run);

function run(
  options: JsonObject & EslintBuilderOptions,
  context: BuilderContext
): Observable<BuilderOutput> {

  return from(getSourceRoot(context))
    .pipe(
      map(sourceRoot => {
        const cli = new CLIEngine({
          cwd: sourceRoot,
          useEslintrc: true,
          fix: options.fix
        });
        const files = options.files;

        const report = cli.executeOnFiles(files);
        const formatter = cli.getFormatter();

        context.logger.info(formatter(report.results));

        if (options.fix) {
          CLIEngine.outputFixes(report);
        }

        return {
          success: report.errorCount === 0
        };
      })
    );
}

async function getSourceRoot(context: BuilderContext) {
  const workspaceHost = workspaces.createWorkspaceHost(new NodeJsSyncHost());
  const { workspace } = await workspaces.readWorkspace(
    context.workspaceRoot,
    workspaceHost
  );
  return workspace.projects.get(context.target.project).sourceRoot;
}
