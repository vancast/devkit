import {
  BuilderContext,
  BuilderOutput,
  createBuilder
} from '@angular-devkit/architect';
import {Observable} from 'rxjs';
import { JsonObject } from '@angular-devkit/core';
import { resolve } from 'path';
import { spawn } from 'child_process';

export interface MochaBuilderOptions {
  files: string;
  codeCoverage?: boolean;
  codeCoverageDir?: string;
  reporters?: string[];
  timeout?: number;
  env?: string;
}

export default createBuilder(run);

function run(
  options: JsonObject & MochaBuilderOptions,
  context: BuilderContext
): Observable<BuilderOutput> {
  const root = resolve(context.workspaceRoot);
  const cwd = resolve(root);
  const nyc = resolve(root, 'node_modules', '.bin', 'nyc');
  const mocha = resolve(root, 'node_modules', '.bin', 'mocha');
  const reportDir = resolve(root, options.codeCoverageDir);
  const reporters = options.reporters.map(r => {
    return '--reporter=' + r;
  });

  return new Observable(observer => {
    let child;
    if (options.codeCoverage) {
      child = spawn(
        nyc,
        [
          `--exclude=${options.files}`,
          ...reporters,
          `--report-dir=${reportDir}`,
          mocha,
          '--recursive',
          `--exit`,
          `--timeout=${options.timeout}`,
          `"${options.files}"`
        ],
        {
          cwd,
          stdio: ['inherit', 'inherit', 'inherit'],
          env: {
            ...process.env,
            NODE_ENV: options.env
          }
        });
    } else {
      child = spawn(
        mocha,
        [
          '--recursive',
          `--exit`,
          `--timeout=${options.timeout}`,
          `"${options.files}"`
        ],
        {
          cwd,
          stdio: ['inherit', 'inherit', 'inherit'],
          env: {
            ...process.env,
            NODE_ENV: options.env
          }
        });
    }

    child.on('close', code => {
      observer.next({
        success: code === 0
      });
      observer.complete();
    });
  });
}
