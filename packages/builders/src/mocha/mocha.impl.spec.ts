import { MochaBuilderOptions } from './mocha.impl';
import { JsonObject } from '@angular-devkit/core';
import { Architect } from '@angular-devkit/architect';
import { getTestArchitect } from '../utils/testing';

jest.mock('child_process');
const {spawn} = require('child_process');

describe('MochaBuilder', () => {
  let testOptions: MochaBuilderOptions & JsonObject;
  let architect: Architect;

  beforeEach(async () => {
    [architect] = await getTestArchitect();

    testOptions = {
      env: 'dev',
      files: 'test/**/*.spec.js',
      reporters: ['text', 'lcov', 'html']
    };
  });

  beforeEach(() => {
    spawn.mockReturnValue({
      on(evt, cb) {
        if (evt === 'close') {
          cb(0);
        }
      }
    });
  });

  afterEach(() => {
    spawn.mockRestore();
  });

  it('should run mocha test', async () => {
    const run = await architect.scheduleBuilder(
      '@vancast/builders:mocha',
      testOptions
    );

    const event = await run.output.toPromise();

    await run.stop();

    expect(event.success).toBe(true);
    expect(spawn.mock.calls[0][0])
      .toBe('/root/node_modules/.bin/mocha');

    expect(spawn.mock.calls[0][2].cwd).toBe('/root');
    expect(spawn.mock.calls[0][2].stdio).toEqual(['inherit', 'inherit', 'inherit']);
    expect(spawn.mock.calls[0][2].env.NODE_ENV).toEqual('dev');

    const args = spawn.mock.calls[0][1];
    expect(args[0]).toBe('--recursive');
    expect(args[1]).toBe('--exit');
    expect(args[2]).toBe('--timeout=2000');
    expect(args[3]).toBe('"test/**/*.spec.js"');
  });

  it('should run mocha test with coverage', async () => {
    const run = await architect.scheduleBuilder(
      '@vancast/builders:mocha',
      {
        ...testOptions,
        codeCoverage: true
      }
    );

    const event = await run.output.toPromise();

    await run.stop();

    expect(event.success).toBe(true);
    expect(spawn.mock.calls[0][0])
      .toBe('/root/node_modules/.bin/nyc');

    const args = spawn.mock.calls[0][1];
    expect(args[0]).toBe('--exclude=test/**/*.spec.js');
    expect(args[1]).toBe('--reporter=text');
    expect(args[2]).toBe('--reporter=lcov');
    expect(args[3]).toBe('--reporter=html');
    expect(args[4]).toBe('--report-dir=/root/coverage');
    expect(args[5]).toBe('/root/node_modules/.bin/mocha');
    expect(args[6]).toBe('--recursive');
    expect(args[7]).toBe('--exit');
    expect(args[8]).toBe('--timeout=2000');
    expect(args[9]).toBe('"test/**/*.spec.js"');
  });
});
