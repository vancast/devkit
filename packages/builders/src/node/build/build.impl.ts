import {
  BuilderContext,
  BuilderOutput,
  createBuilder
} from '@angular-devkit/architect';
import { concat, defer, from, Observable, of } from 'rxjs';
import { JsonObject } from '@angular-devkit/core';
import { resolve } from 'path';
import { debounceTime, map, switchMap, tap } from 'rxjs/operators';
import {remove, copy, pathExistsSync, ensureSymlink} from 'fs-extra';
import watch from 'node-watch';

export interface BuildNodeBuilderOptions {
  outputPath: string;
  sourcePath: string;
  main: string;
  watch?: boolean;
  linkNodeModules?: boolean;
}

export type NodeBuildEvent = BuilderOutput & {
  outfile: string;
};

export default createBuilder(run);

function linkNodeModules(sourceDir, outDir) {
  if (pathExistsSync(resolve(sourceDir, 'node_modules'))) {
    return ensureSymlink(resolve(sourceDir, 'node_modules'), resolve(outDir, 'node_modules'));
  }

  return Promise.resolve();
}

function unlinkNodeModules(outDir) {
  if (pathExistsSync(resolve(outDir, 'node_modules'))) {
    return remove(resolve(outDir, 'node_modules'));
  }

  return Promise.resolve();
}

function copySources(sourceDir: string, outDir: string, link = false): Observable<any> {
  return defer(() => from(
    unlinkNodeModules(outDir)
      .then(() => {
        return copy(
          sourceDir,
          outDir,
          {
            filter: file => {
              return !file.match(/node_modules/);
            }
          }
        );
      })
      .then(() => {
        if (link) {
          return linkNodeModules(sourceDir, outDir);
        }
      })
  ));
}

function removeOutDir(outDir: string): Observable<any> {
  if (!pathExistsSync(outDir)) {
    return of(null);
  }
  return defer(() => from(remove(outDir)));
}


function normalizeOptions(options: JsonObject & BuildNodeBuilderOptions, context: BuilderContext): JsonObject & BuildNodeBuilderOptions {
  const root = resolve(context.workspaceRoot);
  return {
    ...options,
    root,
    outputPath: resolve(root, options.outputPath),
    sourcePath: resolve(root, options.sourcePath),
    main: resolve(root, options.outputPath, options.main)
  };
}

function run(
  options: JsonObject & BuildNodeBuilderOptions,
  context: BuilderContext): Observable<NodeBuildEvent> {

  context.logger.info('Building node application');
  options = normalizeOptions(options, context);

  const tasks = [
    removeOutDir(options.outputPath)
      .pipe(
        switchMap(() => copySources(
          options.sourcePath,
          options.outputPath,
          options.linkNodeModules
        )),
      )
  ];

  if (options.watch) {
    const watch$: Observable<any> = new Observable(observer => {
      watch(options.sourcePath, {recursive: true}, (event, file) => {
        context.logger.info(`${event.toUpperCase()}: ${file}`);
        observer.next(file);
      });
    });

    tasks.push(
      watch$
        .pipe(
          debounceTime(500),
          tap(() => {
            context.logger.info('Detected changes in the source files. Re-building it...');
          }),
          switchMap(() => {
            return copySources(options.sourcePath, options.outputPath, options.linkNodeModules);
          })
        )
    );
  }

  return concat(...tasks)
    .pipe(
      map(() => {
        context.logger.info('Build finished');
        return {
          success: true,
          outfile: options.main
        };
      })
    );
}
