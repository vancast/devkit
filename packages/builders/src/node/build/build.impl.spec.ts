import { BuildNodeBuilderOptions } from './build.impl';
import { JsonObject, schema } from '@angular-devkit/core';
import {
  Architect,
  BuilderRun
} from '@angular-devkit/architect';
import {take} from 'rxjs/operators';
import { getTestArchitect } from '../../utils/testing';

jest.mock('fs-extra');
const {copy, remove, pathExistsSync, ensureSymlink} = require('fs-extra');
jest.mock('node-watch');
const watch = require('node-watch');

describe('NodeBuildBuilder', () => {
  let testOptions: BuildNodeBuilderOptions & JsonObject;
  let architect: Architect;
  let triggerWatch;

  beforeEach(async () => {
    [architect] = await getTestArchitect();

    testOptions = {
      sourcePath: '/src',
      outputPath: '/dist',
      main: 'main.js',
      watch: false,
      linkNodeModules: false
    };
  });

  beforeEach(() => {
    copy.mockReturnValue(Promise.resolve('success'));
    remove.mockReturnValue(Promise.resolve('success'));
    ensureSymlink.mockReturnValue(Promise.resolve('success'));
    pathExistsSync.mockReturnValue(true);
    watch.mockImplementation((src, options, cb) => {
      triggerWatch = function() {
        cb('update', '/src/some/file.js');
      };
    });
  });

  afterEach(() => {
    copy.mockRestore();
    remove.mockRestore();
    ensureSymlink.mockRestore();
    pathExistsSync.mockRestore();
    watch.mockRestore();
  });

  it('should copy source files into dest output path', async () => {
    const run = await architect.scheduleBuilder(
      '@vancast/builders:node-build',
      testOptions
    );

    const event = await run.output.toPromise();

    await run.stop();

    // expect('what ever');
    expect(event.success).toBe(true);
    expect(event.outfile).toBe('/dist/main.js');
  });

  it('should rebuild when changes are detected in the app folder', async () => {
    const run: BuilderRun = await architect.scheduleBuilder(
      '@vancast/builders:node-build',
      {
        ...testOptions,
        watch: true
      }
    );

    setTimeout(() => {
      triggerWatch();
    }, 50);

    await run.output
      .pipe(
        take(2)
      )
      .toPromise()
      .then(() => {
        return run.stop();
      });

    expect(copy).toBeCalledTimes(2);
  });

  it('should link node_modules from source dir', async () => {
    pathExistsSync.mockImplementation(path => {
      return path !== '/dist/node_modules';
    });
    const run: BuilderRun = await architect.scheduleBuilder(
      '@vancast/builders:node-build',
      {
        ...testOptions,
        linkNodeModules: true
      }
    );

    await run.result;
    await run.stop();

    expect(ensureSymlink).toHaveBeenCalledWith('/src/node_modules', '/dist/node_modules');
  });

  it('should not link node_modules if source code has no node_modules folder', async () => {
    pathExistsSync.mockImplementation(path => {
      return path !== '/src/node_modules';
    });

    const run: BuilderRun = await architect.scheduleBuilder(
      '@vancast/builders:node-build',
      {
        ...testOptions,
        linkNodeModules: true
      }
    );

    await run.result;
    await run.stop();

    expect(ensureSymlink).not.toHaveBeenCalled();
  });

  it('should not try to remove out dir if it does not exist', async () => {
    pathExistsSync.mockImplementation(path => {
      return path !== '/dist';
    });
    const run: BuilderRun = await architect.scheduleBuilder(
      '@vancast/builders:node-build',
      {
        ...testOptions,
        linkNodeModules: true
      }
    );

    await run.result;
    await run.stop();

    expect(remove).not.toHaveBeenCalledWith('/dist');
  });
});
