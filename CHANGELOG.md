# [0.2.0](https://gitlab.com/vancast/devkit/compare/v0.1.0...v0.2.0) (2019-06-13)


### Features

* **builders:** code coverage flag for mocha builder ([287ca4a](https://gitlab.com/vancast/devkit/commit/287ca4a))



# [0.1.0](https://gitlab.com/vancast/devkit/compare/v0.0.2...v0.1.0) (2019-06-11)


### Features

* update angular devkit ([04cbd31](https://gitlab.com/vancast/devkit/commit/04cbd31))



## [0.0.2](https://gitlab.com/vancast/devkit/compare/v0.0.1-rc.0...v0.0.2) (2019-05-23)


### Bug Fixes

* tag builds jobs failing ([89a4f07](https://gitlab.com/vancast/devkit/commit/89a4f07))


## 0.0.1 (2019-05-23)


### Bug Fixes

* **builders:** add NODE_ENV variable to the spawn process ([b88e3b9](https://gitlab.com/vancast/devkit/commit/b88e3b9))
* **builders:** eslint build executing only on src folder ([16da0cb](https://gitlab.com/vancast/devkit/commit/16da0cb))
* **builders:** eslint build won't output fixes ([34f11db](https://gitlab.com/vancast/devkit/commit/34f11db))
* **builders:** exclude node_modules when building node applications ([cd0b5f5](https://gitlab.com/vancast/devkit/commit/cd0b5f5))
* **builders:** linting on node_modules ([61e4bda](https://gitlab.com/vancast/devkit/commit/61e4bda))
* **builders:** node build process won't exist ([11e7729](https://gitlab.com/vancast/devkit/commit/11e7729))


### Features

* eslint builder ([f05ca20](https://gitlab.com/vancast/devkit/commit/f05ca20))
* mocha builder ([0706662](https://gitlab.com/vancast/devkit/commit/0706662))
* **builders:** mocha tiemout option ([675b9d3](https://gitlab.com/vancast/devkit/commit/675b9d3))
* **builders:** node-build builder ([e8bfea9](https://gitlab.com/vancast/devkit/commit/e8bfea9))



