#!/usr/bin/env bash

DRY_RUN=false
TAG=latest

for i in "$@"
do
case $i in
  --auth-token=*)
  AUTH_TOKEN="${i#*=}"
  shift
  ;;
  --dry-run)
  DRY_RUN=true
  shift
  ;;
  *)
  TAG=$i
  ;;
esac
done

NPM_DEST=build/packages
ORIG_DIRECTORY=`pwd`


if ! [ -z "$AUTH_TOKEN" ]
then
  npm config set //registry.npmjs.org/:_authToken=$AUTH_TOKEN --global
fi

for package in $NPM_DEST/*/
do

  PACKAGE_DIR="$(basename ${package})"
  cd $NPM_DEST/$PACKAGE_DIR

  PACKAGE_NAME=`node -e "console.log(require('./package.json').name)"`
  VERSION=`node -e "console.log(require('./package.json').version)"`

  echo "Publishing ${PACKAGE_NAME}@${VERSION} --tag ${TAG}"

  if [ "$DRY_RUN" = true ]
  then
    npm publish --tag $TAG --access public --dry-run
  else
    npm publish --tag $TAG --access public
  fi

  cd $ORIGIN_DIRECTORY

 done
