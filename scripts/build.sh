#!/usr/bin/env bash

for i in "$@"
do
case $i in
  --prod)
  ENV=prod
  shift
  ;;
  --test)
  ENV=test
  shift
  ;;
  -e|--env=*)
  ENV="${i#*=}"
  shift
  ;;
  *)
  ENV=dev
  ;;
esac
done

rm -rf build

if [ "$ENV" = "test" ]; then
  ./node_modules/.bin/ngc --project tsconfig.spec.json
else
  ./node_modules/.bin/ngc
fi

rsync -a --exclude=*.ts packages/ build/packages
cp README.md build/packages/builders
cp LICENSE build/packages/builders

echo "Vancast devkit libraries available at build/packages:"
ls build/packages
